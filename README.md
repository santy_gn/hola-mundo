# README #

## Pasos del proceso

- Instalar git 
- Introducir un nombre y un correo con los comandos `$ git config --global user.name "Santiago Guillen"` y 
`$ git config --global user.email "anderguillen@estudiante.edib.es"`
- Crear un repositorio con el comando `$ git init` 
- Se crear� una branch de nuestro repositorio con el comando `$ git branch <main>`
- Queda creada la branch del repositorio
- Registrarse en Bitbucket
- Crear un repositorio en Bitbucket
- Clonar el repositorio recientemente creado en Bitbucket
- Para establecer como proveedor remoto a Bitbucket del repositorio local debemos usar el comando `$ git clone [url]`
- Creamos los dos �ndices con extensi�n js y html
- Usando el comando `$ git add` incluimos los dos �ndices al repositorio, los cuales se a�adir�n autom�tucamente a la main branch 
- Tras esto el proceso se habr� realizado correctamenre